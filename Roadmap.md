Since PeerTubeify is growing in popularity, our ambitions are growing too.

Here are the future planned improvements to the extension:

* Refactor the whole extension to make it more easily expandable.
* Add internationalization
* Handle embeds (Manual mode would show a button to redirect the selected video where automatic mode would automatically redirect the embed to the PeerTube equivalent)
* Redirect users and channels to PeerTube equivalents
* Add tests

Since PeerTubeify is a hobby project, we can't give any ETA. If you want to contribute, feel free to propose your PR's!